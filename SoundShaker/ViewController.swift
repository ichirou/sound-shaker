//
//  ViewController.swift
//  SoundShaker
//
//  Created by Richmond Ko on 07/09/2016.
//  Copyright © 2016 Richmond Ko. All rights reserved.
//

import UIKit
import AVFoundation

class ViewController: UIViewController {

    var player = AVAudioPlayer()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func motionEnded(_ motion: UIEventSubtype, with event: UIEvent?) {
        if event?.subtype == UIEventSubtype.motionShake {
            
            let soundArray = ["big_swish_with_ding", "cartoon_boing", "cow_moo", "fast_zap", "frog_nighttime_ambience", "knife_throw_with_impact_and_vibration", "outdoor_dog_kennel_with_dogs_barking", "schoolbell", "single_gun_shot_with_ricochet", "single_roar_from_lion", "steel_guitar_sneak_up_and_down_accent"]
            
            let randomNumber = Int(arc4random_uniform(UInt32(soundArray.count)))
            
            let fileLocation = Bundle.main.path(forResource: soundArray[randomNumber], ofType: "mp3")
            do {
                try player = AVAudioPlayer(contentsOf: URL(fileURLWithPath: fileLocation!))
                player.play()
            } catch {
                
            }
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

